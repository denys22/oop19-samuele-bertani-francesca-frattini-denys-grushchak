package model.initialization;


import java.util.Random;
import java.util.stream.Stream;

import controller.StartingMode;
import model.entity.cell.Cell;
import model.entity.cell.standard.prefabCell.CellCreator;
import model.world.World;
import utilities.Pair;

/**
 * 
 * Initialize world of simulation.
 *
 */
public class WorldInitialization {
    private final World world;
    private final CellCreator creator;
    /**
     * @param world of current simulation.
     * @param creator of new cells.
     */
    public WorldInitialization(final World world, final CellCreator creator) {
        this.world = world;
        this.creator = creator;
    }
    /**
     * Initialize world.
     * @param startingMode starting mode of simulation.
     */
    public void initialize(final StartingMode startingMode) {
        switch (startingMode) {
        case SINGLE_PHOTOSYNTHESIS_CELL:
            final Cell cell = creator.newAllPhotosyntesisCell(world.getWorldWidth() / 2, world.getWorldHeight() / 4);
            world.putCell(cell.getX(), cell.getY(), cell);
            break;
        case RANDOM_GENOME_CELLS:
            randomicInitialization();
            break;
        default:
            throw new IllegalStateException("Starting mode is not implemented");
        }
    }

    private void randomicInitialization() {
        final int numCells = (int) Math.sqrt((double) (world.getWorldHeight() * world.getWorldWidth())) * 3;
        final Random ran = new Random();
        Stream.generate(() -> new Pair<Integer, Integer>(ran.nextInt(world.getWorldWidth()), ran.nextInt(world.getWorldHeight())))
                .filter(f -> world.getSquare(f.getX(), f.getY()).getEntity().isEmpty())
                .limit(numCells)
                .forEach(e -> world.putCell(e.getX(), e.getY(), creator.newAllRandomCell(e.getX(), e.getY())));
    }

}
