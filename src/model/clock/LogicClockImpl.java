package model.clock;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * Class that implements a clock from the instant of instantiation to 00:00 .
 * 
 */
public class LogicClockImpl implements LogicClock {

    private final LocalDateTime start;
    private static final int SECONDS_IN_MINUTE = 60;

    /**
     * Builder that initializes the clock from the current instant to 00:00.
     */
    public LogicClockImpl() {
        super();
        start = LocalDateTime.now();
    }

    @Override
    public final String getSecond() {
        return String.format("%02d", getIntSecond());
    }

    @Override
    public final String getMinute() {
        return String.format("%02d", getIntMinute());
    }

    @Override
    public final int getIntSecond() {
        final LocalDateTime current = LocalDateTime.now();
        return (int) (Duration.between(start, current).toSeconds() % SECONDS_IN_MINUTE);
    }

    private int getIntMinute() {
        final LocalDateTime current = LocalDateTime.now();
        return (int) Duration.between(start, current).toMinutes();
    }

}
