package model;
import model.runner.RunnerImp;
import model.world.WorldImp;

/**
 * starts the simulation.
 *
 */
public class ModelImpl implements Model {

    private final RunnerImp starter;
    private int iteration = 0;

    /**
     * default constructor for class ModelImpl.
     * @param screen the world to work with
     */
     public ModelImpl(final WorldImp screen) {
         starter = new RunnerImp(screen);
     }

     /**
      * method that runs all cells in the simulation.
      */

    public final void start() {
        iteration++;
        starter.aliveCells();
        starter.deadCells();
    }

    /**
     * 
     * @return number of iterations 'til now.
     */
    public final int getIteration() {
        return iteration;
    }
}
