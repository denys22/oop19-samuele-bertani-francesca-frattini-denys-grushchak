package model.genome.genes.internals;


import model.direction.DirectionDecryptor;

import model.entity.cell.standard.CellStandard;
import model.genome.genes.Gene;
import model.genome.genes.steps.GenomeStepEnum;

/**
 * 
 * Change the direction of a cell.
 *
 */
public class ChangeDirectionGene implements Gene {

    @Override
    public final void launch(final CellStandard cell) {
         final int parameter = cell.getGeneValueWithOffsetAndJump(GenomeStepEnum.ONE.getStep());
         cell.setDirection(DirectionDecryptor.getDirection(parameter));
    }

    @Override
    public final String getDescription() {
        return "Change direction";
    }

}
