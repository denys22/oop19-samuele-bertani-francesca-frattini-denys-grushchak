package model.genome.genes;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.direction.DirectionEnum;
import model.entity.cell.standard.CellStandard;
import model.entity.cell.standard.prefabCell.CellCreator;
import model.entity.cell.standard.prefabCell.CellCreatorImpl;
import model.genome.decryptor.GeneDecryptor;
import model.genome.decryptor.GeneDecryptorImpl;
import model.genome.factory.GenesEnum;
import model.genome.factory.GenesFactoryImpl;
import model.genome.factory.GenesManagerImpl;
import model.genome.genes.externals.CellFamily;
import model.genome.genes.externals.DeathGene;
import model.genome.genes.externals.ReproductionGene;
import model.properties.cells.CellDataBuilderImpl;
import model.properties.defaultdata.CellsDefaultUtils;
import model.properties.genes.GenesDataBuilderImpl;
import model.world.World;
import model.world.WorldImp;

/**
 * 
 * Test for Externals genes.
 *
 */
public class TestExternalGenes implements CellFamily {
    private World world;
    private CellCreator creator;
    private GeneDecryptor decryptor;

    /**
     * initialization.
     */
    @BeforeEach
    public void init() {
        this.world = new WorldImp(10, 10);

        this.decryptor = new GeneDecryptorImpl(
                CellsDefaultUtils.NUMBER_OF_GENES.getDafaultValue(),
                new GenesManagerImpl(
                        new GenesFactoryImpl(
                                new GenesDataBuilderImpl(this.world).build())));

        this.creator = new CellCreatorImpl(new CellDataBuilderImpl(this.decryptor).build());
    }

    /**
     * Test correct work of {@link CellFamily}.
     */
    @Test
    public final void testCellFamilyControl() {
        final CellStandard cellA = creator.newAllPhotosyntesisCell(0, 0);
        final CellStandard cellB = creator.newAllPhotosyntesisCell(0, 1);

        assertTrue(areRelatives(cellA, cellB), "must be relatives");

        for (int i = 0; i <= CellFamily.TOLERANCE; i++) {
            cellB.setGene(i, -1);
        }

        assertFalse(areRelatives(cellA, cellB), "they are not relatives");
    }

    /**
     * Test correct work of {@link DeathGene}.
     */
    @Test
    public final void testDeathGene() {
        final CellStandard cell = creator.newAllPhotosyntesisCell(1, 1);
        this.world.putCell(cell.getX(), cell.getY(), cell);
        this.decryptor.getGeneOfEnum(GenesEnum.DEATH).launch(cell);
        assertFalse(cell.isActive(), "cell is dead");
    }

    /**
     * Test correct work of genes that make a cell eats another cell.
     */
    @Test
    public final void testEatingGenes() {
        final CellStandard cellA = creator.newAllPhotosyntesisCell(0, 0);
        final CellStandard cellB = creator.newAllPhotosyntesisCell(1, 0); // cellB is on East of CellA
        this.world.putCell(cellA.getX(), cellA.getY(), cellA);
        this.world.putCell(cellB.getX(), cellB.getY(), cellB);
        //set genes of cellA because it will be use them for determinate direction of attack.
        cellA.setGene(1, DirectionEnum.EAST.getIndex());
        cellA.setGene(2, DirectionEnum.EAST.getIndex());
        //cellB must remain relatives to cellA, so set same genes.
        cellB.setGene(1, DirectionEnum.EAST.getIndex());
        cellB.setGene(2, DirectionEnum.EAST.getIndex());
        cellA.decrementEnergy(100);
        // save current level of energy for.
        final int energy = cellA.getEnergy();

        //lunch the gene that don't attack relatives
        this.decryptor.getGeneOfEnum(GenesEnum.EAT_ALL_EXCEPT_RELATIVES).launch(cellA);
        assertTrue(cellB.isActive(), "cellB is not eaten");
        assertTrue(this.world.isThereAnything(cellB.getX(), cellB.getY()), "cellB must remain at its position");

        //lunch the gene that permit attack all.
        this.decryptor.getGeneOfEnum(GenesEnum.EAT_ALL).launch(cellA);
        assertFalse(cellB.isActive(), "cellB is eaten");
        assertFalse(this.world.isThereAnything(cellB.getX(), cellB.getY()), "cellB must be remove from world");
        assertTrue(energy < cellA.getEnergy(), "cellA receive energy");
    }

    /**
     * Test for altruistic genes.
     */
    @Test
    public final void testAltruisticGenes() {
        final CellStandard cellA = creator.newAllPhotosyntesisCell(0, 0);
        final CellStandard cellB = creator.newAllPhotosyntesisCell(1, 0); // cellB is on East of CellA
        this.world.putCell(cellA.getX(), cellA.getY(), cellA);
        this.world.putCell(cellB.getX(), cellB.getY(), cellB);

        cellA.setDirection(DirectionEnum.EAST);

        cellB.decrementEnergy(cellB.getEnergy() / 2);

        final int minerals = 100;
        cellA.incrementMineral(minerals);
        cellB.incrementMineral(minerals / 2);

        this.decryptor.getGeneOfEnum(GenesEnum.SHARE_ENERGY).launch(cellA);
        assertEquals(cellA.getEnergy(), cellB.getEnergy(), "cellA share energy with cellB");
        assertNotSame(cellA.getMineral(), cellB.getMineral(), "minerals was not shared");

        this.decryptor.getGeneOfEnum(GenesEnum.SHARE_MINERALS).launch(cellA);
        assertEquals(cellA.getMineral(), cellB.getMineral(), "CellA share minerals with cellB");

        this.decryptor.getGeneOfEnum(GenesEnum.GIVE_RESOURCES).launch(cellA);
        assertTrue(cellA.getEnergy() < cellB.getEnergy(), "CellA give part of energy to CellB");
        assertTrue(cellA.getMineral() < cellB.getMineral(), "CellA five part of minerals to CellB");
    }

    /**
     * Test of {@link ReproductionGene}.
     */
    @Test
    public final void testReproductionGene() {
        final CellStandard cell = creator.newAllPhotosyntesisCell(0, 0);
        this.world.putCell(cell.getX(), cell.getY(), cell);
        cell.setDirection(DirectionEnum.EAST);

        assertFalse(world.isThereAnything(1, 0), "this place is free");
        this.decryptor.getGeneOfEnum(GenesEnum.REPRODUCTION).launch(cell);
        assertTrue(world.isThereAnything(0, 0), "cell mother remain as its place");
        assertTrue(world.isThereAnything(1, 0), "cell child was placed there");
    }
}
