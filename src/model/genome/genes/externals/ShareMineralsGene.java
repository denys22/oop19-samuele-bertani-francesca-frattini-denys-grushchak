package model.genome.genes.externals;

import model.entity.cell.standard.CellStandard;
import model.world.World;

/**
 * 
 * If in direction of the cell is an another cell with less minerals, the cell
 * share mineral with another for make their minerals equal.
 * 
 */
public class ShareMineralsGene extends GiveResourcesGene {
    /**
     * @param world the current world of simulation.
     */
    public ShareMineralsGene(final World world) {
        super(world);
    }

    /**
     * Make a cell share minerals with an another cell.
     * @param cell that shares minerals.
     * @param receiverCell cell that receives minerals.
     */
    @Override
    protected final void share(final CellStandard cell, final CellStandard receiverCell) {
        if (cell.getMineral() > receiverCell.getMineral()) {
            final int minerals = (cell.getMineral() - receiverCell.getMineral()) / 2;
            cell.decrementMineral(minerals);
            receiverCell.incrementMineral(minerals);
        }
    }

    @Override
    public final String getDescription() {
        return "Share minerals";
    }

}
