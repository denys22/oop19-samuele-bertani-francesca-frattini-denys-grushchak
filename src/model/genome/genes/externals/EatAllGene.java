package model.genome.genes.externals;

import java.util.Optional;

import model.direction.Direction;
import model.direction.DirectionDecryptor;
import model.entity.Entity;
import model.entity.EntityTypeNameEnum;
import model.entity.cell.CellTypeNameEnum;
import model.entity.cell.cellDead.CellDead;
import model.entity.cell.standard.CellStandard;
import model.entity.cell.standard.obtainable.EnergyTypeEnum;
import model.genome.genes.steps.GenomeStepEnum;
import model.world.World;

/**
 * 
 * A cell try to eat another cell in an direction.
 *
 */
public class EatAllGene extends AbstractExternalGene {
    private static final float ENERGY_PORTION = 0.5F;
    private final int nutritionOfDeadCell;
    /**
     * @param world the current world of simulation.
     * @param nutritionOfDeadCell the quantity of energy that receive cell if it ate an dead cell.
     */
    public EatAllGene(final World world, final int nutritionOfDeadCell) {
        super(world);
        this.nutritionOfDeadCell = nutritionOfDeadCell;
    }

    @Override
    public final void launch(final CellStandard cell) {
        //take a parameter and transform it in direction
        final Direction direction = DirectionDecryptor.getDirection(
                cell.getGeneValueWithOffsetAndJump(GenomeStepEnum.ONE.getStep())); 

        final Optional<Entity> something = getEntityInDirection(cell.getX(), cell.getY(), direction);

        if (something.isPresent() && something.get().getEntityType().equals(EntityTypeNameEnum.CELL)) {
            final CellTypeNameEnum cellToAttackType = castToCell(something.get()).getCellTypeName();

            switch (cellToAttackType) {
            case CELL_STANDARD_ALIVE:
                final CellStandard victim = 
                (CellStandard) tryCastToSpecificCell(something.get(), CellTypeNameEnum.CELL_STANDARD_ALIVE).get(); 
                eatStandardCell(cell, victim);
                break;
            case CELL_DEAD:
                final CellDead deadCell = 
                (CellDead) tryCastToSpecificCell(something.get(), CellTypeNameEnum.CELL_DEAD).get(); 
                eatDeadCell(cell, deadCell);
                break;
            default: 
                //make genome jump
                cell.getGeneValueWithOffsetAndJump(GenomeStepEnum.ONE.getStep()); 
            }
        } else {
            cell.getGeneValueWithOffsetAndJump(GenomeStepEnum.TWO.getStep()); 
        }
    }

    /**
     * Make cell to try eat the victim.
     * @param cell that want eats  another cell.
     * @param victim the cell that will be attacked.
     */
    protected void eatStandardCell(final CellStandard cell, final CellStandard victim) {
        battle(cell, victim);
    }

    /**
     * Make cell to eat deadCell.
     * @param cell that want eat dead cell.
     * @param deadCell that  will be eaten.
     */
    private void eatDeadCell(final CellStandard cell, final CellDead deadCell) {
        cell.incrementEnergy(nutritionOfDeadCell, EnergyTypeEnum.EATING);
        getWorld().removeCell(deadCell.getX(), deadCell.getY());
    }

    /**
     * Battle between the cells.
     * @param cell that attacks.
     * @param victim that defends.
     */
    protected void battle(final CellStandard cell, final CellStandard victim) {
        //if the cell has less minerals than victim.
        if (cell.getMineral() < victim.getMineral()) {
            cell.decrementMineral(cell.getMineral()); // cell lost all its minerals
            victim.decrementMineral(cell.getMineral()); // victim lost quantity of minerals equal to the quantity that lost cell. 
            // if the cell has no more than double the energy than the victim has minerals
            if (cell.getEnergy() / 2 < victim.getMineral()) { 
                victim.decrementMineral(cell.getEnergy() / 2); // victim lost its minerals in battle.
                getWorld().makeCellDeath(cell.getX(), cell.getY()); // cell lost the battle and died
                cell.deactive();
            } else { //otherwise the cell kills and eats victim
                cell.decrementEnergy(victim.getMineral() * 2); // cell lost energy in battle.
                makeEatenVictim(cell, victim);
            }
        //if the cell has more minerals than victim. so cell kills and eats victim.
        } else {
            cell.decrementMineral(victim.getMineral()); // cell lost minerals in battle.
            makeEatenVictim(cell, victim);
        }
    }

    private void makeEatenVictim(final CellStandard cell, final CellStandard victim) {
        final int energyToAdd = Math.round(ENERGY_PORTION * victim.getEnergy());
        cell.incrementEnergy(energyToAdd + nutritionOfDeadCell, EnergyTypeEnum.EATING); //the cell eats victim.
        getWorld().removeCell(victim.getX(), victim.getY());
        victim.deactive();
    }

    /**
     * if the class will be extended this method must be override.
     */
    @Override
    public String getDescription() {
        return "Eat all";
    }
}
