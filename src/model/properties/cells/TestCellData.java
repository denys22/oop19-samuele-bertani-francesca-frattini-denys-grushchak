package model.properties.cells;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import model.genome.decryptor.GeneDecryptorImpl;
import model.genome.factory.GenesFactoryImpl;
import model.genome.factory.GenesManager;
import model.genome.factory.GenesManagerImpl;
import model.properties.defaultdata.CellsDefaultUtils;
import model.properties.genes.GenesData;
import model.properties.genes.GenesDataBuilderImpl;
import model.world.WorldImp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * 
 * Tests of {@link CellDataBuilderImpl}.
 *
 */
public class TestCellData {
    private static final  int NUM_GENES = 36; 
    private CellDataBuilder builder;

    /**
     * Before each.
     */
    @BeforeEach
    public void init() {
        final GenesData genesData = new GenesDataBuilderImpl(new WorldImp(10, 10)).build();
        final GenesManager genesManager = new GenesManagerImpl(new GenesFactoryImpl(genesData));
        this.builder =  new CellDataBuilderImpl(new GeneDecryptorImpl(NUM_GENES, genesManager));
    }

    /**
     * Test correct working of the builder.
     */
    @Test
    public void testCellBuilder() {
        final int age = 666;
        System.out.println(this.builder);
        final CellData data = this.builder.setMaxAge(age)
                                          .build();

        assertEquals(CellsDefaultUtils.TURN_COST.getDafaultValue(), data.getTurnCost(), "test turn cost");
        assertEquals(CellsDefaultUtils.GENOME_SIZE.getDafaultValue(), data.getGenomeSize(), "test genome size");
        assertEquals(age, data.getMaxAge(), "test max age");
    }

    /**
     * Test correct launch of IllegalArgumenException and IllegalStateException.
     */
    @Test
    public void testCellBuilderExceptions() {
        assertThrows(IllegalArgumentException.class, () -> this.builder.setTurnCost(-1).build());
        assertThrows(IllegalArgumentException.class, () -> this.builder.setNumberOfGenes(1000).build());
        assertThrows(IllegalArgumentException.class, () -> this.builder.setNumberOfGenes(10).build()); //is not multiple of 8
        assertThrows(IllegalArgumentException.class,
                () -> this.builder.setMaxAge(CellsDefaultUtils.MAX_AGE.getMaximumValue() + 1).build());
        this.builder.build();
        assertThrows(IllegalStateException.class, () -> {
            this.builder.setMaxAge(1000);
        });
        assertThrows(IllegalStateException.class, () -> {
            this.builder.setGenomeSize(1000);
        });
        assertThrows(IllegalStateException.class, () -> {
            this.builder.setTurnCost(1);
        });
    }
}
