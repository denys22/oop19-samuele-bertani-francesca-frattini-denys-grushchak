package model.entity.cell.standard.age;

/**
 * 
 * The age of the cell (number of cycle of the created cell).
 *
 */
public interface Age {
    /**
     * a simple get.
     * @return the age
     */
    int getAge();
    /**reset the age of the cell.
     * 
     */
    void resetAge();
}
