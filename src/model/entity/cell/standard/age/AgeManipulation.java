package model.entity.cell.standard.age;

/**
 * 
 *  Methods the cell can use to work on age.
 *
 */
public interface AgeManipulation extends Age {

    /**
     * increment with a +1.
     */
    void increment();
    /**
     * 
     * @return <code>true</code> if is dead for old age.
     */
    boolean isDead();

}
