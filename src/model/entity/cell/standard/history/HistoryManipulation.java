package model.entity.cell.standard.history;

import model.entity.cell.standard.obtainable.EnergyTypeEnum;

/**
 * 
 * Methods the cell can use to work on history.
 *
 */
public interface HistoryManipulation extends History {
    /**
     * Add received energy int he history.
     * @param value amount of energy.
     * @param energyType type of energy.
     */
    void addEnergy(int value, EnergyTypeEnum energyType);
}
