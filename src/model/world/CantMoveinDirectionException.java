package model.world;

/**
 * exception throws when a cell can't move in a given direction.
 *
 */
public class CantMoveinDirectionException extends RuntimeException {

    /**
     * automatically generated.
     */
    private static final long serialVersionUID = 1L;

    /**
     * construct a new CantMoveinDirectionException with a given error message.
     * @param message
     *                  the error message
     */
    public CantMoveinDirectionException(final String message) {
        super(message);
    }
}
