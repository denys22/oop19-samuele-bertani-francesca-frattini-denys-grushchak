package model.language;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

/**
 * Test to verify the correctness of the bundle translation.
 * 
 */
class TestLanguage {
    private final LanguageSet l = new Language();
    private static final String KEY_CELL_ALIVE = "CellA";
    private static final String KEY_CELL_DEAD = "CellD";
    private static final String KEY_EMPTY = "Empty";
    private static final String KEY_STONE = "Stone";

    /**
     * Test to check the return of correctly translated strings_ENGLISH_.
     */
    @Test
    public final void englishTest() {
        final String errorMassage = "The translation should be wrong";

        l.setcurrentbundle(Languages.ENGLISH);
        assertEquals("LIVING CELL", Language.getkeyofbundle(KEY_CELL_ALIVE), "Wrong translation of the CellA key");
        assertNotEquals("CELLULA VIVA", Language.getkeyofbundle(KEY_CELL_ALIVE), errorMassage);
        assertNotEquals("CÉLULA VIVA", Language.getkeyofbundle(KEY_CELL_ALIVE), errorMassage);

        assertEquals("DEAD CELL", Language.getkeyofbundle(KEY_CELL_DEAD), "Wrong translation of the 'CellD' key");
        assertNotEquals("CELLULA MORTA", Language.getkeyofbundle(KEY_CELL_DEAD), errorMassage);
        assertNotEquals("CÉLULA MUERTA", Language.getkeyofbundle(KEY_CELL_DEAD), errorMassage);

        assertEquals("EMPTY", Language.getkeyofbundle(KEY_EMPTY), "Wrong translation of the 'Empty' key");
        assertNotEquals("VUOTA", Language.getkeyofbundle(KEY_EMPTY), errorMassage);
        assertNotEquals("VACÍO", Language.getkeyofbundle(KEY_EMPTY), errorMassage);

        assertEquals("STONE", Language.getkeyofbundle(KEY_STONE), "Wrong translation of the 'Stone' key");
        assertNotEquals("PIETRA", Language.getkeyofbundle(KEY_STONE), errorMassage);
        assertNotEquals("PIEDRA", Language.getkeyofbundle(KEY_STONE), errorMassage);
    }

    /**
     * Test to check the return of correctly translated strings _ITALIANO_.
     */
    @Test
    public final void italianTest() {
        l.setcurrentbundle(Languages.ITALIAN);
        assertEquals("CELLULA VIVA", Language.getkeyofbundle(KEY_CELL_ALIVE), "Wrong translation of the 'CellA' key");
        assertEquals("CELLULA MORTA", Language.getkeyofbundle(KEY_CELL_DEAD), "Wrong translation of the 'CellD' key");
        assertEquals("VUOTA", Language.getkeyofbundle(KEY_EMPTY), "Wrong translation of the 'Empty' key");
        assertEquals("PIETRA", Language.getkeyofbundle(KEY_STONE), "Wrong translation of the 'Stone' key");
    }

    /**
     * Test to check the return of correctly translated strings _ESPANOL_.
     */
    @Test
    public final void epanolTest() {
        l.setcurrentbundle(Languages.ESPANOL);
        assertEquals("CÉLULA VIVA", Language.getkeyofbundle(KEY_CELL_ALIVE), "Wrong translation of the 'CellA' key");
        assertEquals("CÉLULA MUERTA", Language.getkeyofbundle(KEY_CELL_DEAD), "Wrong translation of the 'CellD' key");
        assertEquals("VACÍO", Language.getkeyofbundle(KEY_EMPTY), "Wrong translation of the 'Empty' key");
        assertEquals("PIEDRA", Language.getkeyofbundle(KEY_STONE), "Wrong translation of the 'Stone' key");
    }

}
