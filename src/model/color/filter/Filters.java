package model.color.filter;

import java.util.Arrays;
import java.util.Optional;

/**
 * Enumeration that describes the different types of color filters with which you can view the world and its entities.
 *
 */
public enum Filters {
    /**
     * Describes the type of filter that returns the color of standard cells according to their age.
     */
    AGE("Age", 0), 
    /**
     * Describes the type of filter that returns the color of standard cells according to their energy.
     */
    ENERGY("Energy", 1), 
    /**
     * Describes the type of filter that restores the color of standard cells based on the nutrient 
     * they have used during their lifetime.
     */
    NUTRITION("Nutrition", 2);

    private String name;
    private int value;

    /**
     * Constructor that initializes an enum by giving it a name (also the bundol key) and a value.
     * @param name enum name
     * @param value enum value 
     */
    Filters(final String name, final int value) { 
        this.name = name;
        this.value = value;
    }

    /**
     * Method that returns the name of the respective enum.
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Method that returns the value of the respective enum.
     * @return value
     */
    public int getValue() {
        return value;
    }

    /**
     * Calculate the enum Filters corresponding to name.
     * @param name containing the name of the filter you want to search for
     * @return enum Filter corresponding to the name inserted
     * @throws IllegalArgumentException if the name entered does not correspond to any existing enum Filters
     */
    public static Filters getEnum(final String name) {
       final Optional<Filters> filter = Arrays.asList(Filters.values()).stream().filter(x -> x.getName().equals(name)).findFirst();
       if (filter.isPresent()) {
           return filter.get();
       } else {
           throw new IllegalArgumentException("IL NOME INSERITO NON CORRISPONDE A NESSUN ENUM");
       }
    }

    /**
     * Calculate the enum Filters corresponding to value.
     * @param value containing the int value of the filter you want to search for
     * @return enum Filter corresponding to the value inserted
     * @throws IllegalArgumentException if the value entered does not correspond to any existing enum Filters
     */
    public static Filters getEnum(final int value) {
        final Optional<Filters> filter = Arrays.asList(Filters.values()).stream().filter(x -> x.getValue() == value).findFirst();
        if (filter.isPresent()) {
            return filter.get();
        } else {
            throw new IllegalArgumentException("IL VALORE INSERITO NON CORRISPONDE A NESSUN ENUM");
        }
    }
}
