package model.color.filter;

import java.awt.Color;

import controller.file.data.input.DataProgramUtils;
import model.entity.cell.standard.CellStandard;

/**
 * Class that manages the color of standard cells according to their energy. Starting with the default or user-set hue 
 * and changing the saturation according to energy.
 * 
 */
public class EnergyFilter extends FilterImpl {

    private static final int MAX_ENERGY = DataProgramUtils.getMaxEnergy();
    private static final float HUE = DataProgramUtils.getHueColor();

    /**
     * Calculates the color of the STANDARD_CELL passed as a parameter based on its energy.
     * Starting from a shade chosen by the user, its saturation decreases as the energy decreases.
     * BRIGHT COLORS-> HIGHER ENERGY
     * OFF COLORS-> LESS ENERGY
     * @param cellstandard standard cell of which you want to know the color
     * @return cell color 
     */
    @Override
    protected final Color getCellStandardColor(final CellStandard cellstandard) {
        return Color.getHSBColor(HUE, saturation(cellstandard.getEnergy()), 1);
    }

    private float saturation(final int energy) {
        return (float) energy / MAX_ENERGY;
    }

}
