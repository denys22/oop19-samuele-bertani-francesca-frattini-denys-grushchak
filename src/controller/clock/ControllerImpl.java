package controller.clock;

import javax.swing.JLabel;

import controller.file.data.input.DataProgramUtils;
import model.Model;
import model.clock.LogicClock;
import model.clock.LogicClockImpl;
import view.menu.data.setting.AddElemValue;
import view.start.world.ApplicationFrame;
import view.start.world.UpdateSpecific;
import view.start.world.UpdateWorld;

/**
 * Control class that manages the update of application window elements such as world, clock, specifications, 
 * and stops the update as soon as the stop button is pressed.
 *
 */
public class ControllerImpl implements Controller {

    private static LogicClock time;
    private static final int UPDATE_FRAME = DataProgramUtils.getUpDateView();
    private final JLabel clockPanel;
    private final UpdateWorld viewWorld;
    private final AddElemValue<Boolean> stop;
    private final UpdateSpecific specific;
    private static final int MILLS_PAUSE = 40;

    /**
     * Builder that initializes all the elements of the application window that will be to be synchronized.
     * @param mainFrame the main application window
     */
    public ControllerImpl(final ApplicationFrame mainFrame) {
        super();
        this.stop = mainFrame.getButton();
        this.viewWorld = mainFrame.getWorld();
        this.clockPanel = mainFrame.getClockView();
        this.specific = mainFrame.getSpecific();
    }

    @Override
    public final void start(final Model t) {
        int thisSecond;
        int lastSecond = 1;
        time = new LogicClockImpl();
        while (stop.getValue()) {
            clockPanel.setText(time.getMinute() + " : " + time.getSecond());
            thisSecond = time.getIntSecond();
            if (thisSecond % UPDATE_FRAME == 0 && thisSecond != lastSecond) {
                lastSecond = thisSecond;
                thisSecond = time.getIntSecond();
                updateFrame(t.getIteration());
            }
            try {
                Thread.sleep(MILLS_PAUSE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            t.start();
        }
        viewWorld.enablePressCells();
    }

    private void updateFrame(final int contDay) {
        viewWorld.updateStatus();
        specific.updateSpecific(viewWorld.getCoutUpDate(), contDay);
    }

}
