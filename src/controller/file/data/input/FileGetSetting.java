package controller.file.data.input;

import controller.StartingMode;
import model.color.filter.Filters;

/**
 * Interface that will be implemented classes that load from file the data inherent to the parameters set by the user.
 */
public interface FileGetSetting {

    /**
     * Method that returns color filter type to display the world set by the user.
     * @return color filter type
     */
    Filters getColorFilter();

    /**
     * Method that returns the number of cells present in the height of the world set by the user.
     * @return number of cells in height
     */
    int getHeightWorld();

    /**
     * Method that returns the number of cells present in the width of the world set by the user.
     * @return number of cells in width
     */
    int getWidthWorld();

    /**
     * Method that returns the hue used by color filters set by the user.
     * @return hue
     */
    float getHue();

    /**
     * Method that returns the maximum energy that can be stored by a single cell set by the user.
     * @return maximum energy
     */
    int getMaxEnery();

    /**
     * @return the maxSunLight of the cell
     */
    int getSunLight();

    /**
     * Method that returns the number of seconds each time you want to update the graphical user interface set by the user.
     * @return number of seconds each time you want to update the graphical user interface
     */
    int getUpDateView();

    /**
     * Method that returns the percentage of rows from the bottom where the totality of the ore will be present, set by the user.
     * @return percentage of lines whit mineral
     */
    float getMineralDepth();

    /**
     * Method that returns the percentage of the lines from the top where the totality of the sun will be present,
     * set by the user.
     * @return percentage of lines whit sun
     */
    float getSunPenetration();

    /**
     * Method that gives back the maximum age, so the maximum number of days a cell can live set by the user.
     * @return maximum age
     */
    int getMaxAge();

    /**
     * Method that returns the value of the length of the genome, therefore the number of genes that constitute it,
     *  set by the user.
     * @return length of the genome
     */
    int getSizeGenoma();

    /**
     * Method that returns the starting mode in which the program will take place either with random gene or with gene 
     * given by photosynthesis set by the user.
     * @return starting mode
     */
    StartingMode getMod();

}
