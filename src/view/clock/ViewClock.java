package view.clock;

import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import model.language.Language;
import view.menu.data.setting.AddElemValue;
import view.menu.data.setting.DimensionComponent;
import view.start.world.UpdateLanguageElem;

/**
 * Class that implements a panel that displays a clock screen in "00:00" format.
 *
 */
public class ViewClock extends JPanel implements AddElemValue<JLabel>, UpdateLanguageElem {

    private static final long serialVersionUID = 4501339558652460035L;
    private final JLabel text = new JLabel();
    private static final int SCALE_FONT = 5;
    private final JPanel mainpanel = new JPanel();

    /**
     * Constructor initializing the clock display.
     */
    public ViewClock() {
        super();
        final TitledBorder titleBorder = new TitledBorder(Language.getkeyofbundle("Time"));
        mainpanel.setBorder(titleBorder);
        mainpanel.setPreferredSize(DimensionComponent.CLOCK_DISPLAY.getDimension());
        mainpanel.add(setJLabel(), BorderLayout.CENTER);
        this.add(mainpanel);
    }

    @Override
    public final JPanel getElem() {
        return this;
    }

    @Override
    public final JLabel getValue() {
        return text;
    }

    @Override
    public final void updateLanguageElem() {
        final TitledBorder titleBorder = new TitledBorder(Language.getkeyofbundle("Time"));
        mainpanel.setBorder(titleBorder);
    }

    private JPanel setJLabel() {
        final JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        text.setFont(new Font("Agency FB", Font.PLAIN, sizeText()));
        panel.add(text);
        return panel;
    }

    private int sizeText() {
        return DimensionComponent.CLOCK_DISPLAY.getDimension().width / SCALE_FONT;
    }

}
