package view.start.world;

import java.awt.Component;
import java.util.HashMap;
import java.util.Map;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import model.language.Language;
import model.world.CellInfos;
import view.menu.data.setting.AddElem;
import view.menu.data.setting.DimensionComponent;

/**
 * Class that implements a panel with information about the world.
 *
 */
public class SpecificWorld extends JPanel implements UpdateSpecific, AddElem, UpdateLanguageElem {

    private static final long serialVersionUID = 745320494116267779L;
    private final Map<String, JLabel> labels = initMap();
    private final CellInfos worldInfo;
    private final JPanel panel = new JPanel();
    private final JLabel contUpdateFrame = new JLabel();
    private final JLabel contCellAlive = new JLabel();
    private final JLabel contCellDeath = new JLabel();
    private final JLabel contDay = new JLabel();
    private final JLabel contAge = new JLabel();
    private final JLabel contGenes = new JLabel();
    private final JLabel contEnergy = new JLabel();
    private final JLabel contPhoto = new JLabel();
    private final JLabel contMin = new JLabel();
    private final JLabel contCarn = new JLabel();
    private final JLabel contAltr = new JLabel();

    /**
     * Constructor that generates a JPanel and adds the strings and values that you want to display in the specifications.
     * @param worldInfo the specifications related to the world
     */
    public SpecificWorld(final CellInfos worldInfo) {
        super();
        this.worldInfo = worldInfo;
        panel.setBorder(new TitledBorder(Language.getkeyofbundle("Specific")));
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setPreferredSize(DimensionComponent.SPECIFIC_DISPLAY.getDimension());
        panel.add(addCont(labels.get("CountUpDate"), contUpdateFrame));
        panel.add(addCont(labels.get("CountDay"), contDay));
        panel.add(addCont(labels.get("CountCellAlive"), contCellAlive));
        panel.add(addCont(labels.get("CountCellDeath"), contCellDeath));
        panel.add(addCont(labels.get("AgeMedium"), contAge));
        panel.add(addCont(labels.get("NGenesMedium"), contGenes));
        panel.add(addCont(labels.get("EnergyMedium"), contEnergy));
        panel.add(addCont(labels.get("EnergyPhot"), contPhoto));
        panel.add(addCont(labels.get("EnergyMin"), contMin));
        panel.add(addCont(labels.get("EnergyCarn"), contCarn));
        panel.add(addCont(labels.get("EnergyAltr"), contAltr));
        this.add(panel);
    }

    @Override
    public final Component getElem() {
        return this;
    }

    @Override
    public final void updateSpecific(final int contUpdateWorld, final int contDayVal) {
        contUpdateFrame.setText(" " + (contUpdateWorld - 1));
        contDay.setText(" " + contDayVal);
        contCellAlive.setText(" " + worldInfo.getAliveCellsNumber());
        contCellDeath.setText(" " + worldInfo.getDeadCellsNumber());
        contAge.setText(" " + worldInfo.getMediumAge());
        contGenes.setText(" " + worldInfo.getMediumNumberofGenes());
        contEnergy.setText(" " + worldInfo.getMediumTotalEnergy());
        contPhoto.setText(" " + worldInfo.getMediumPercPhotosyntesisEnergy() + " %");
        contMin.setText(" " + worldInfo.getMediumPercMineralEnergy() + " %");
        contCarn.setText(" " + worldInfo.getMediumPercEatingEnergies() + " %");
        contAltr.setText(" " + worldInfo.getMediumPercAltruismEnergy() + " %");
    }

    @Override
    public final void updateLanguageElem() {
        panel.setBorder(new TitledBorder(Language.getkeyofbundle("Specific")));
        labels.entrySet().stream()
        .parallel()
        .forEach(l -> l.getValue().setText(Language.getkeyofbundle(l.getKey())));
    }


    private JPanel addCont(final JLabel descLabel, final JLabel contLabel) {
        final JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        panel.setAlignmentX(LEFT_ALIGNMENT);
        panel.add(descLabel);
        panel.add(contLabel);
        return panel;
    }

    private Map<String, JLabel> initMap() {
        final Map<String, JLabel> map = new HashMap<>();
        map.put("CountUpDate", new JLabel(Language.getkeyofbundle("CountUpDate")));
        map.put("CountCellAlive", new JLabel(Language.getkeyofbundle("CountCellAlive")));
        map.put("CountCellDeath", new JLabel(Language.getkeyofbundle("CountCellDeath")));
        map.put("CountDay", new JLabel(Language.getkeyofbundle("CountDay")));
        map.put("AgeMedium", new JLabel(Language.getkeyofbundle("AgeMedium")));
        map.put("NGenesMedium", new JLabel(Language.getkeyofbundle("NGenesMedium")));
        map.put("EnergyMedium", new JLabel(Language.getkeyofbundle("EnergyMedium")));
        map.put("EnergyPhot", new JLabel(Language.getkeyofbundle("EnergyPhot")));
        map.put("EnergyMin", new JLabel(Language.getkeyofbundle("EnergyMin")));
        map.put("EnergyCarn", new JLabel(Language.getkeyofbundle("EnergyCarn")));
        map.put("EnergyAltr", new JLabel(Language.getkeyofbundle("EnergyAltr")));
        return map;
    }
}
