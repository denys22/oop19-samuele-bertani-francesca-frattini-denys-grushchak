package view.menu.data.setting;

import java.util.LinkedList;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;

import model.language.Language;
import model.properties.defaultdata.ViewDefaultUtils;

/**
 * Class that manages a panel containing checkBoxes and a button to set the number of frames 
 * after which you want to update the view.
 *
 */
public class AddUpDateView extends JPanel implements AddElemValue<Integer> {

    private static final long serialVersionUID = -3059697665791907178L;
    private Integer value = ViewDefaultUtils.UPDATE_VIEW.getDafaultValue();
    private final JCheckBox cbDefault = new JCheckBox(value.toString());
    private final JCheckBox cbOther = new JCheckBox(Language.getkeyofbundle("ClickSet"));
    private final JLabel description = new JLabel(Language.getkeyofbundle("FrequenceUpDateView"));

    /**
     * Constructor that implements a panel to set the seconds after which the view must be set. 
     * Consisting of check box and the possibility to set a value chosen by keyboard provided that it is acceptable.
     */
    public AddUpDateView() {
        super();
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        description.setAlignmentX(CENTER_ALIGNMENT);
        this.add(description);
        this.add(addBox());
        this.add(new JSeparator());
    }

    @Override
    public final Integer getValue() {
        return value;
    }

    @Override
    public final JPanel getElem() {
        return this;
    }

    private JPanel addBox() {
        final List<Integer> numberFrame = new LinkedList<>(List.of(1, 2, 3, 5, 10));
        final JPanel panel = new JPanel();
        final List<JCheckBox> checkboxs = new LinkedList<>();

        numberFrame.forEach(n -> {
            JCheckBox cb;
            if (n == value) {
                cb = cbDefault;
                defaltvalue();
            } else {
            cb = new JCheckBox(n.toString());
            }
            checkboxs.add(cb);
        });
        checkboxs.add(cbOther);

        checkboxs.forEach(ckb -> {
            ckb.addActionListener(e -> {
                final JCheckBox cb = (JCheckBox) e.getSource();
                checkboxs.forEach(b -> b.setSelected(false));
                if (cb.equals(cbOther)) {
                    tempFrame();
                } else {
                    updateValue(cb, Integer.parseInt(cb.getText()));
                    cbOther.setText(Language.getkeyofbundle("ClickSet"));
                }
            });
            panel.add(ckb);
        });
        return panel;
    }

    private void tempFrame() {
        final String input = JOptionPane.showInputDialog(null, Language.getkeyofbundle("InfoMex"), Language.getkeyofbundle("TitleInfo"),
                JOptionPane.QUESTION_MESSAGE);
        boolean exceptionVerificate = false;
        try {
            value = Integer.parseInt(input); 
            if (value >= ViewDefaultUtils.UPDATE_VIEW.getMinimumValue() 
                    && value <= ViewDefaultUtils.UPDATE_VIEW.getMaximumValue()) {
                cbOther.setSelected(true);
            } else {
                throw new NumberFormatException();
            }
        } catch (NumberFormatException e) {
            exceptionVerificate = true;
            defaltvalue();
            JOptionPane.showMessageDialog(null, Language.getkeyofbundle("ErrorMex"), Language.getkeyofbundle("TitleError"),
                    JOptionPane.ERROR_MESSAGE);
        }
        if (!exceptionVerificate) {
            cbOther.setText(input + " (" + Language.getkeyofbundle("ClickChange") + ")");
        }
    }

    private void updateValue(final JCheckBox cb, final int n) {
        cb.setSelected(true);
        value = n;
    }

    private void defaltvalue() {
        updateValue(cbDefault, ViewDefaultUtils.UPDATE_VIEW.getDafaultValue());
    }
}
