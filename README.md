# CyberBiology

Il gruppo si pone come obiettivo di realizzare un’applicazione che riproduce un simulatore di una popolazione di cellule.
Crediamo che il progetto sia molto attuale in quanto può avere applicazioni nel reparto genetico e, in generale, quello scientifico, 
mostrando come, in una popolazione di cellule, i geni possano variare nelle generazioni successive per adattarsi all’ambiente che li 
circonda, secondo la legge di Darwin.
Le cellule crescono e si riproducono sfruttando diverse forme di energia, le quali sono: fotosintesi, fagocitazione ed energia minerale.